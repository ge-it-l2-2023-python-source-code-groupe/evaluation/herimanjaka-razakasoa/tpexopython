from tools.console import clear
def choix_exo_variables():
    clear()
    pass
    print(f'''
                
    {'_'*30}
    Chapitres Variables
    -------------------
    Selectionner l'exo :
                
    1 : exo2_11_1
    2 : exo2_11_2
    3 : exo2_11_3
                
    ''')
    exo = input("veuillez selectionner un numero de l'exo (ou q pour revenir): ")
    if(exo == '1'):
        from chapitres.variables.exo2_11_1 import run
        run()
    elif(exo == '2'):
        from chapitres.variables.exo2_11_2 import run
        run()
    elif(exo == '3'):
        from chapitres.variables.exo2_11_3 import run
        run()
    elif exo == 'q':
        return
    else:
        print("Saisi errone")
        
    precedent = input(f'''
{'_'*30}

Revenir sur le menu Chapitre ? [y/n]: ''')
    if(precedent != 'y'):
        choix_exo_variables()
    


'''
    Fonction pour gerer les choix de l'exo sur l'affichage
'''
def choix_exo_affichage():
    clear()
    pass
    print(f'''
                
    {'_'*30}
        Chapitre : Affichages 
        --------------------
        1- exo3_6_1            4- exo3_6_4
        2- exo3_6_2            5- exo3_6_5
        3- exo3_6_3
                    
                    ''')
    
    exo = input("veuillez selectionner un numero de l'exo (ou q pour revenir): ")
    if(exo == '1'):
        from chapitres.affichage.exo3_6_1 import run
        run()
    elif(exo == '2'):
        from chapitres.affichage.exo3_6_2 import run
        run()
    elif(exo == '3'):
        from chapitres.affichage.exo3_6_3 import run
        run()
    elif(exo == '4'):
        from chapitres.affichage.exo3_6_4 import run
        run()
    elif(exo == '5'):
        from chapitres.affichage.exo3_6_5 import run
        run()
    elif exo == 'q':
        return
    else:
        print("Saisi errone")
        
    precedent = input(f'''
{'_'*30}

Revenir sur le menu Chapitre ? [y/n]: ''')
    if(precedent != 'y'):
        choix_exo_affichage()
        
def choix_exo_listes():
    clear()
    pass
    print(f'''
                
    {'_'*30}
        Chapitre : Listes
        --------------------
        1- exo4_10_1           4- exo4_10_4
        2- exo4_10_2           5- exo4_10_5
        3- exo4_10_3           6- exo4_10_6
                    
                    ''')
    
    exo = input("veuillez selectionner un numero de l'exo (ou q pour revenir): ")
    if(exo == '1'):
        from chapitres.liste.exo4_10_1 import run
        run()
    elif(exo == '2'):
        from chapitres.liste.exo4_10_2 import run
        run()
    elif(exo == '3'):
        from chapitres.liste.exo4_10_3 import run
        run()
    elif(exo == '4'):
        from chapitres.liste.exo4_10_4 import run
        run()
    elif(exo == '5'):
        from chapitres.liste.exo4_10_5 import run
        run()
    elif(exo == '6'):
        from chapitres.liste.exo4_10_6 import run
        run()
    elif exo == 'q':
        return
    else:
        print("Saisi errone")
        
    precedent = input(f'''
{'_'*30}

Revenir sur le menu Chapitre ? [y/n]: ''')
    if(precedent != 'y'):
        choix_exo_listes()

def choix_exo_boucles():
    clear()
    pass
    print(f'''
                
    {'_'*30}
        Chapitre : Boucles
        --------------------
        1- exo5_4_1           6- exo5_4_6             11- exo5_4_11
        2- exo5_4_2           7- exo5_4_7             12- exo5_4_12
        3- exo5_4_3           8- exo5_4_8             13- exo5_4_13
        4- exo5_4_4           9- exo5_4_9             14- exo5_4_14
        5- exo5_4_5          10- exo5_4_10
                    ''')
    
    exo = input("veuillez selectionner un numero de l'exo (ou q pour revenir): ")
    if(exo == '1'):
        from chapitres.boucles.exo5_4_1 import run
        run()
    elif(exo == '2'):
        from chapitres.boucles.exo5_4_2 import run
        run()
    elif(exo == '3'):
        from chapitres.boucles.exo5_4_3 import run
        run()
    elif(exo == '4'):
        from chapitres.boucles.exo5_4_4 import run
        run()
    elif(exo == '5'):
        from chapitres.boucles.exo5_4_5 import run
        run()
    elif(exo == '6'):
        from chapitres.boucles.exo5_4_6 import run
        run()
    elif(exo == '7'):
        from chapitres.boucles.exo5_4_7 import run
        run()
    elif(exo == '8'):
        from chapitres.boucles.exo5_4_8 import run
        run()
    elif(exo == '9'):
        from chapitres.boucles.exo5_4_9 import run
        run()
    elif(exo == '10'):
        from chapitres.boucles.exo5_4_10 import run
        run()
    elif(exo == '11'):
        from chapitres.boucles.exo5_4_11 import run
        run()
    elif(exo == '12'):
        from chapitres.boucles.exo5_4_12 import run
        run()
    elif(exo == '13'):
        from chapitres.boucles.exo5_4_13 import run
        run()
    elif(exo == '14'):
        from chapitres.boucles.exo5_4_14 import run
        run()
    elif exo == 'q':
        return
    else:
        print("Saisi errone")
        
    precedent = input(f'''
{'_'*30}

Revenir sur le menu Chapitre ? [y/n]: ''')
    if(precedent != 'y'):
        choix_exo_boucles()

def choix_exo_tests():
    clear()
    pass
    print(f'''
                
    {'_'*30}
        Chapitre : tests
        --------------------
        1- exo6_7_1           6- exo6_7_6
        2- exo6_7_2           7- exo6_7_7
        3- exo6_7_3           8- exo6_7_8
        4- exo6_7_4           9- exo6_7_9
        5- exo6_7_5          10- exo6_7_10
                    ''')
    
    exo = input("veuillez selectionner un numero de l'exo (ou q pour revenir): ")
    if(exo == '1'):
        from chapitres.tests.exo6_7_1 import run
        run()
    elif(exo == '2'):
        from chapitres.tests.exo6_7_2 import run
        run()
    elif(exo == '3'):
        from chapitres.tests.exo6_7_3 import run
        run()
    elif(exo == '4'):
        from chapitres.tests.exo6_7_4 import run
        run()
    elif(exo == '5'):
        from chapitres.tests.exo6_7_5 import run
        run()
    elif(exo == '6'):
        from chapitres.tests.exo6_7_6 import run
        run()
    elif(exo == '7'):
        from chapitres.tests.exo6_7_7 import run
        run()
    elif(exo == '8'):
        from chapitres.tests.exo6_7_8 import run
        run()
    elif(exo == '9'):
        from chapitres.tests.exo6_7_9 import run
        run()
    elif(exo == '10'):
        from chapitres.tests.exo6_7_10 import run
        run()
    elif exo == 'q':
        return
    else:
        print("Saisi errone")
        
    precedent = input(f'''
{'_'*30}

Revenir sur le menu Chapitre ? [y/n]: ''')
    if(precedent != 'y'):
        choix_exo_tests()