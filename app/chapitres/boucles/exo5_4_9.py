def run():
  print('''
        #exo5_4_9
        ---------
        Sujet:
        ------
        
        Créez un script qui dessine un triangle comme celui-ci :
          *
          **
          ***
          ****
          *****
          
          Reponse:
          --------
        ''')

  long = int(input('Veuillez entrer la longueur du triangle gauche : '))

  for i in range(long+1):
      print(f'{" "*(long-i)}{"*"*i}')