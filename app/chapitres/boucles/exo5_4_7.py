def run():
  print('''
        #exo5_4_7 Triangle:
        ---------
        Sujet:
        -----
        
        Créez un script qui dessine un triangle comme celui-ci :
          *
          **
          ***
          ****
          *****
          
          Reponse:
          --------
          
        ''')

  long = int(input('Veuillez entrer la longueur du triangle: '))

  '''
      le nombre de i correspond au nombre de caractere pour former un triangle donc si 0 => pas de triangle
  '''

  for i in range(long+1):
      print(f'{"*"*i}')
