def run():
  print('''
        exo5_4_8:
        --------
        Sujet:
        ------
        
        Créez un script qui dessine un triangle comme celui-ci :
          ******
          *****
          ****
          ***
          **
          *
        ''')

  long = int(input('Veuillez entrer la longueur du triangle inverse: '))

  for i in range(long+1):
      print(f'{"*"*(long-i)}')