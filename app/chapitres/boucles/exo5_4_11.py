def run():
  print('''
        #5.4.11 Parcours de matrice:
        ----------------------------
          Sujet:
          -----
          Imaginons que l'on souhaite parcourir tous les éléments d'une matrice carrée,
      c'est-à-dire d'une matrice qui est constituée d'autant de lignes que de colonnes.
      Créez un script qui parcourt chaque élément de la matrice et qui affiche le numéro de
      ligne et de colonne uniquement avec des boucles for.
          
        ''')

  n = int(input('Entrez le rang de la matrice : '))

  for i in range(n):
      for j in range(n):
          print(i+1 , j+1 ,sep=' ')