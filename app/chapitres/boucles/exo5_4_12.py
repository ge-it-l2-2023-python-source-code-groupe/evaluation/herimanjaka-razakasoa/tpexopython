def run():
    print('''
        #exo5_4_12 Parcours de demi-matrice sans la diagonale:
        ------------------------------------------------------
        Sujet:
        ------
        Créez un script qui affiche le numéro de ligne et de colonne, puis la taille de la
        matrice N x N et le nombre total de cases parcourues.
        
        
        Concevez une seconde version à partir du script précédent, où cette fois on n'affiche
    plus tous les couples possibles mais simplement la valeur de N, et le nombre de
    cases parcourues. Affichez cela pour des valeurs de N allant de 2 à 10.

        ''')
    n = int(input('Entrez le rang de la matrice : '))
    count = 0;

    for i in range(n):
        for j in range(i+1,n):
            print(i+1 , j+1 , sep=' ')
            count+=1

    print(f'Pour cette matrice de taille {n}*{n}: le nombre de case parcouru est : {count}')

    print('''
        Seconde version du script:
        -------------------------
        
        ''')
    for n in range(2,11):
        case_parcouru = 0
        for i in range(n):
            for j in range(i+1,n):
                case_parcouru+=1
        print(f'Pour une matrice de taille {n}*{n} : On a parcouru {case_parcouru} case(s)')
        
    print('''
        Une formule generale pour trouver cette case ?
        
        On constate que le resultat est : 1 , 3 , 6, ...
            on peut en tirer que la case parcouru pour le rang suivant est quivalent au case parcouru precedent + le rang precedent.
            Formule => Case  = Case precedent + taille precedent dont la premiere case est 1 pour la taille 2*2
        ''')