def run():
  print('''
        #exo5_4_6
        ---------
        
        Sujet:
        -----
        Avez les fonctions list() et range(), créez la liste entiers contenant les nombres
      entiers pairs de 2 à 20 inclus.
      Calculez ensuite le produit des nombres consécutifs deux à deux de entiers en
      utilisant une boucle. Exemple pour les premières itérations :
      8
      24
      48
      [...]
      
      Reponse:
      --------
        ''')
  my_list = list(range(2,21,2))

  for i in range(len(my_list)-1):
      print(my_list[i]*my_list[i+1])

