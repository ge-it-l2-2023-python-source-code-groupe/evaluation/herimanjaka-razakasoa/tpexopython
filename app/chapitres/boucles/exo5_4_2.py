def run():
    print(f'''
        #exo5_4_2
        Sujet:
        -----
        
        Constituez une liste semaine contenant les 7 jours de la semaine.
    Écrivez une série d'instructions affichant les jours de la semaine (en utilisant une
    boucle for), ainsi qu'une autre série d'instructions affichant les jours du week-end
    (en utilisant une boucle while).

        ''')

    semaine = ['lundi','mardi','mercredi','jeudi','vendredi','samedi','dimanche']

    print('''
    #for method
    -----------
        for jour in semaine[:5]:
            print(jour)
        
        ''')
    for jour in semaine[:5]:
        print(f". {jour}")


    print('''
    #while method
    -------------
        i=-2
        while i<0:
            print(f'. {semaine[i]}')
        ''')
    i=-2
    while i<0:
        print(f'. {semaine[i]}')
        i+=1