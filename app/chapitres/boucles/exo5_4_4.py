def run():
      print('''
            #exo5_4_4
            ---------
            Sujet:
            ------
            
            Soit impairs la liste de nombres [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21]. Écrivez
            un programme qui, à partir de la liste impairs, construit une liste pairs dans laquelle
            tous les éléments de impairs sont incrémentés de 1.
            
      pairs = [x+1 for x in impairs] => 
            ''', end=' ')

      impairs = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21]

      pairs = [x+1 for x in impairs]
      print(pairs)