def run():
      print(f'''
      {'_'*30}
      
            #exo5_4_14 Suite de Fibonacci
            ----------------------------
            Sujet : Créez un script qui construit une liste fibo avec les 15 premiers termes de la suite de
      Fibonacci puis l'affiche.

      {'_'*30}
            ''')

      fibo = [0,1]

      for i in range(2,16):
            fibo.append(fibo[i-2] + fibo[i-1])  
      
      print(fibo)

      {'_'*30}

      print('''
            Améliorez ce script en affichant, pour chaque élément de la liste fibo avec 𝑛 > 1, le
      rapport entre l'élément de rang 𝑛 et l'élément de rang 𝑛 − 1. Ce rapport tend-il vers
      une constante ? Si oui, laquelle ?
            ''')

      for i in range(2,16):
            print(fibo[i]/fibo[i-1])

      {'_'*30}

      print('''
            On constate que ce rapport tend vers un nombre constante 1,618 (Nombre d'or)
            ''')