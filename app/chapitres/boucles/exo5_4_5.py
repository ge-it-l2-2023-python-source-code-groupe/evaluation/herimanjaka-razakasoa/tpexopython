def run():
  print('''
        #exo5_4_5
        ---------
        Sujet:
        ------
        
        Voici les notes d'un étudiant [14, 9, 6, 8, 12]. Calculez la moyenne de ces notes.
      Utilisez l'écriture formatée pour afficher la valeur de la moyenne avec deux
      décimales.
      
        Comme c'est une exo pour les boucles , j'ai utilise for, 
        mais on a une solution alternative :
        print(sum(notes)/len(notes)) dont sum prend une liste et renvoie la somme 
        ----------------
        ''')

  result = 0
  notes = [14, 9, 6, 8, 12]
  for note in notes:
      result+=note
  print(f'La moyenne des notes est : {result/len(notes):.2f}')
