def run():
      print('''
            #exo5_4_10:
            ----------
            Sujet:
            ------
            
            Créez un script pyra.py qui dessine une pyramide comme celle-ci : 
              *
             ***
            *****
            
            Reponse:
            -------
            
            ''')

      n  = int(input('Veuillez entrer la longueur du pyramide: '))

      '''
      Le nombre de '*' suit 2*i -1 , (il suffit d'ajouter des espaces qui correspond au nombre de i)
      '''
      for i in range(n+1):
            print(f"{' '*(n-i)}{'*'*(2*i-1)}")