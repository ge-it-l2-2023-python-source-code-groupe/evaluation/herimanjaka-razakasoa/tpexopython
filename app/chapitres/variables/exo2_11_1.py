def run():
  print("""
    *********************************
      #exo2_11_1
      
      Nombre de Friedman:
      test si une expression est un 
      nombre de friedman

      Par exemple, 347 est un nombre
      de Friedman car il peut s'écrire
      sous la forme 4 + 7

      ********************************
""")
  print(f" 7 + 3\u2076  = {7 + 3**6} est un nombre de Friedman")
  print(f" (3+4)\u2073  = {(3+4)**3} est un nombre de Friedman")
  print(f" 3\u2076 - 5  = {3**6 - 5} n'est pas un nombre de Friedman")
  print(f" (1 + 2\u2078)*5  = {(1 + 2)*5} n'est pas un nombre de Friedman")
  print(f" (2 + 1\u2078)\u2077 = {(2 + 1**8)**7} est un nombre de Friedman")