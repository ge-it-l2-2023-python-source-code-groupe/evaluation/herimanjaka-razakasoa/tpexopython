def run():
      print("""
      ********************************
            #exo2_11_2
            
            Prediction de resultat:
            avant de verifier sur
            l'interpreteur

      ********************************
      """)

      print(f"""
      Expression: (1+2)**3
      Prediction : 27
      Interpreteur : {(1+2)**3}

            """)

      print("----------------------------")

      print(f"""
      Expression: "Da"*4
      Prediction : "DaDaDaDa"
      Interpreteur : {"Da"*4}

            """)
      
      print("----------------------------")

      print(f"""
      Expression: "Da"+3
      Prediction : "Da3"
      Interpreteur : "Erreur : interpreteur ne peut pas concatener un str avec un int"

      """)

      print("----------------------------")

      print(f"""
      Expression: ("Pa"+"La") * 2
      Prediction : "PaLaPaLa"
      Interpreteur : {("Pa"+"La") * 2}

            """)

      print("----------------------------")

      print(f"""
      Expression: ("Da"*4) / 2
      Prediction : "DaDa"
      Interpreteur : unsupported operand type(s) for /: 'str' and 'int'

            """)

      print("----------------------------")

      print(f"""
      Expression: 5/2
      Prediction : "2.5"
      Interpreteur : {5/2}

            """)

      print("----------------------------")

      print(f"""
      Expression: 5/2
      Prediction : "2"
      Interpreteur : {5//2}

            """)


      print("----------------------------")

      print(f"""
      Expression: 5%2
      Prediction : "1"
      Interpreteur : {5%2}

            """)