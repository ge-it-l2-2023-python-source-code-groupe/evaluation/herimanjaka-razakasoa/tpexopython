def run():
  print(f'''
      {'*'*30}
        #exo4_10_3
        ----------
        Sujet :
        -------
        Affichez la table de multiplication par 9 en une seule commande avec les
  instructions range() et list().
      {'*'*30}
        '''
        )

  '''
  list comprehension :
      pour tous les x de 1 a 9 on affiche x et son produit avec 9
      
  '''
  table = [f'{x} * {9} = {x*9}' for x in range(1,10)]

  print(f'''
        Reponse:
        --------
        {table}
        '''
        )