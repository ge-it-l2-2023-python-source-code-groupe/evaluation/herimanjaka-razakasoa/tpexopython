def run():
      print(f'''
            {'*'*30}
            #exo4_10_5
            
            Sujet:
            ------
            
            Écrire un programme python qui créé une liste semaine qui comprend les jours de la
      semaine, puis à l’aide de parcours successifs de la liste effectuer les actions
      suivantes :
      1- Afficher la liste semaine
      2- Afficher la valeur de semaine[4]
      3- Échanger les valeurs de la première et de la dernière case de cette liste
      4- Afficher 12 fois la valeur du dernier élément de la liste
      {'*'*30}
            ''')

      week = ['lundi','mardi','mercredi','jeudi','vendredi','samedi','dimanche']

      print(f'''
            Reponse:
            -------
            
            liste de la semaine => {week}
            valeur de la semaine 4 => {week[4]}
            ''')

      week[0],week[-1]=week[-1],week[0]

      print(f'''
            (week[0],week[-1]=week[-1],week[0]) => {week}

            
      Pour afficher 12 fois la derniere valeur:
            week[-1]*12 => {week[-1]*12}
            Oui c'est lundi parcequ'on a inverse la liste
            
            {'*'*30}
            ''')