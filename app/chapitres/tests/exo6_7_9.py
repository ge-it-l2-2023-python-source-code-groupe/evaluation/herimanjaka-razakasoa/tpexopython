def run():
      print('''
            #exo6_7_9 Implementation des 2 methode pour trouver le nombre premier inférieurs à 100
            ---------
            
            ''')

      print('''
            Methode 1:
            ----------
            ''')
      nombre_premier_methode1 = []

      for nombre in range(2,100):
            diviseur = 0
            for i in range(1,nombre+1):
                  if nombre % i == 0:
                        diviseur += 1
            if diviseur == 2:
                  nombre_premier_methode1 += [nombre]

      print(f'''
            Avec la premiere methode on a trouve {len(nombre_premier_methode1)}
            qui sont {nombre_premier_methode1}
            
            {'_'*30}
            
            ''')

      print('''
            Methode 2:
            ----------
            ''')

      nombre_premier_methode2 = [2]

      for nb in range(3,100):
            est_premier = True
            for premier in nombre_premier_methode2:
                  if nb % premier == 0:
                        est_premier = False
            if est_premier:
                  nombre_premier_methode2 += [nb]
                  
      print(f'''
            Avec la deuxieme methode on a trouve {len(nombre_premier_methode2)}
            qui sont {nombre_premier_methode2}
            
            {'_'*30}
            
            ''')
