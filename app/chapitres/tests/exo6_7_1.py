def run():
    print('''
        #exo6_7_1 Jour de la semaine :
        ------------------------------
        
        Sujet :
        En utilisant une boucle, écrivez chaque jour de la semaine ainsi que les messages
    suivants :
    ● Au travail s'il s'agit du lundi au jeudi ;
    ● Chouette c'est vendredi s'il s'agit du vendredi ;
    ● Repos ce week-end s'il s'agit du samedi ou du dimanche.


        ''')
    semaine = ['lundi','mardi','mercredi','jeudi','vendredi','samedi','dimanche']

    for jour in semaine:
        if jour == 'vendredi':
            print("chouette c'est vendredi")
        elif jour == 'samedi' or jour=='dimanche':
            print(f'Repos ce week-end on est {jour}')
        else:
            print(f"Au travail on est encore {jour}")