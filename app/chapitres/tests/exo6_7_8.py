def run():
    print('''
        #exo6_7_8 Attribution de la structure secondaire des acides
    aminés d'une protéine

        Sujet :
        -------
        créez un script qui teste, pour chaque acide aminé, s'il est ou
    non en hélice et affiche les valeurs des angles phi et psi et le message adapté est en
    hélice ou n'est pas en hélice.

        ''')

    angles = [[48.6, 53.4], [-124.9, 156.7], [-66.2, -30.8],
                [-58.8, -43.1], [-73.9, -40.6], [-53.7, -37.5],
                [-80.6, -26.0], [-68.5, 135.0], [-64.9, -23.5],
                [-66.9, -45.5], [-69.6, -41.0], [-62.7, -37.5],
                [-68.2, -38.3], [-61.2, -49.1], [-59.7, -41.1]]


    for phi, psi in angles:
        print(f'[{phi},{psi}]', end=' ')
        '''
            Deviation plus ou moins 30 degre pris en compte 
        '''
        if (phi > -67 and phi < -47) or (psi > -37 and psi < -17) or (psi > -107 and psi < -77):
            print("est en hélice\n")
        else:
            print("n'est pas en hélice\n")

