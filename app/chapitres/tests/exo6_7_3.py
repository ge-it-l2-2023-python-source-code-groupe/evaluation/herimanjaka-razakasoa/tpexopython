def run():
    print('''
        #exo6_7_3 Minimum d'une liste
        Sans utiliser la fonction min
        
        Sans utiliser cette fonction, créez
    un script qui détermine le plus petit élément de la liste [8, 4, 6, 1, 5].
        ''')

    elt = [8, 4, 6, 1, 5]
    min = elt[0]

    '''
        Posons min= le premier element, 
        on compare les valeur a cet min et on le change si on en trouve un.
        
    '''
    for i in range(1,len(elt)):
        if elt[i] < min:
            min = elt[i]
            
    print(f'''
        elt  = [8, 4, 6, 1, 5]
        Le min dans cet elt est {min}
        ''')