def run():
    print('''
        #exo6_7_2 Séquence complémentaire d'un brin d'ADN
        -------------------------------------------------
        
        complementaire d'un brin d'adn:
        
        ''')

    adn  = ["A", "C", "G", "T", "T", "A", "G", "C", "T", "A", "A", "C", "G"]
    print(f'adn => {adn}')


    for i in range(len(adn)):
        if adn[i] == "A":
            adn[i] = 'T'
        elif adn[i] == 'T':
            adn[i] = 'A'
        elif adn[i] == 'C':
            adn[i] = 'G'
        else:
            adn[i] = 'C'

        
    print(f'''
    complementaire => {adn}

    J'ai transforme l'adn precedent comme indique au sujet,
    j'ai juste affiche les 2 pour la lisibilite
        ''')