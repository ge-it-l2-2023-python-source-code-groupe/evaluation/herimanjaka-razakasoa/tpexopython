def run():
    print(f'''
    {'_'*30}
        #exo6_7_6 Nombres pairs
        -----------------------
        Sujet :
        Construisez une boucle qui parcourt les nombres de 0 à 20 et qui affiche les
    nombres pairs inférieurs ou égaux à 10 d'une part, et les nombres impairs
    strictement supérieurs à 10 d'autre part.

    {'_'*30}
        ''')

    for i in range(21):
        if i%2 == 0 and i<=10:
            print(i)
        elif i%2 == 1 and i>10:
            print(i)