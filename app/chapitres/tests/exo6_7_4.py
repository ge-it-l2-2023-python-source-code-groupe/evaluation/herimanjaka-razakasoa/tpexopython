def run():
      print('''
            #exo6_7_4 Fréquence des acides aminés
            -------------------------------------
            acide = ["A", "R", "A", "W", "W", "A", "W", "A", "R", "W", "W", "R", "A", "G"]

            ''')
      acide = ["A", "R", "A", "W", "W", "A", "W", "A", "R", "W", "W", "R", "A", "G"]

      '''
      Je peux utiliser un boucle et incrementer chaque compteur, mais comme c'est pas mentionne
      j'ai facilite un peu la tache en utilisant la methode count d'une liste (acide en l'occurence)
      '''
      print(f'''
            Reponse: 
            
            Frequence de A : {acide.count('A')} sur {len(acide)} acide amines
            
            Frequence de R : {acide.count('R')} sur {len(acide)} acide amines
            
            Frequence de G : {acide.count('G')} sur {len(acide)} acide amines
            
            Frequence de W : {acide.count('W')} sur {len(acide)} acide amines
            ''')