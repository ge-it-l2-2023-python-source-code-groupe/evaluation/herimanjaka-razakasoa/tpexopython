def run():
    print(f'''
    ****************************************************************************
        #exo3_6_1

        Ouvrez l'interpréteur Python et tapez l'instruction 1+1. Que se passe-t-il ?
        Écrivez la même chose dans un script test.py que vous allez créer avec un éditeur
        de texte. Exécutez ce script en tapant python test.py dans un shell. Que se
        passe-t-il ? Pourquoi ? Faites en sorte d'afficher le résultat de l'addition 1+1 en
        exécutant le script dans un shell.

    *****************************************************************************

        Reponse :
        - Sur l'interpreteur : taper 1+1 affiche 2
        - Si on ecrit 1+1 dans un fichier .py et 
    qu'on l'execute , il se passe rien car, 
    c'est juste une instruction qui ne demande pas d'afficher le resulta
    par contre , dans un interpreteur, le fait de taper le nom d'un variable
    affiche son valeur ou l'evalue avant de l'afficher

    ''')
        
    print(f"Pouf afficher le resultat de 1+1 dans un fichier.py il faut alors indiquer a l'interpreteur de l'afficher avec print : print(1+1) => {1+1}")