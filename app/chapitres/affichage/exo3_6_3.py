def run():
    print(f'''
    {'*'*30}
    #exo3_6_3

    Sur le modèle de l'exercice précédent, générez en une ligne de code un brin d'ADN
    poly-A (AAAA...) de 20 bases suivi d'un poly-GC régulier (GCGCGC...) de 40 bases.

    {'*'*30}

    Reponse:
    -------

    'A'*20 'GC'*20  =>  {'A'*20}{'GC'*20}

    ''')
        