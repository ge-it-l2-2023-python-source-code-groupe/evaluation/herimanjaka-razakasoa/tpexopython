def run():
    print(f'''
    {'*'*30}

    #exo3_6_2

    Générez une chaîne de caractères représentant un brin d'ADN poly-A (c'est-à-dire qui
    ne contient que des bases A) de 20 bases de longueur, sans taper littéralement
    toutes les bases.

    {'*'*30}

    Reponse:

    'A'*20 => {'A'*20}

    ''')

