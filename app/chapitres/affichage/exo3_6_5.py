def run():
    print(f'''
    {'*'*30}
    #exo3_6_5

    Sujet:
    -----

    Dans un script percGC.py, calculez un pourcentage de GC avec l'instruction suivante :
    perc_GC = ((4500 + 2575)/14800)*100
    Ensuite, affichez le contenu de la variable perc_GC à l'écran avec 0, 1, 2 puis 3
    décimales sous forme arrondie en utilisant l'écriture formatée et les f-strings. On
    souhaite que le programme affiche la sortie suivante :
    Le pourcentage de GC est 48%
    Le pourcentage de GC est 47.8%
    Le pourcentage de GC est 47.80%
    Le pourcentage de GC est 47.804 %

    {'*'*30}

    '''
    )

    perc_GC = ((4500 + 2575)/14800)*100

    print(f'''
    Reponse :
    ---------

    0 => Le pourcentage de GC est : {perc_GC:.0f}
    1 => Le pourcentage de GC est : {perc_GC:.1f}
    2 => Le pourcentage de GC est : {perc_GC:.2f}
    3 => Le pourcentage de GC est : {perc_GC:.3f}

    '''
    )