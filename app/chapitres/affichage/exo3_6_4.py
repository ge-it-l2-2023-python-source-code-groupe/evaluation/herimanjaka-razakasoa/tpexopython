def run():
    print(f'''
    {'*'*30}
    #exo3_6_4

    En utilisant l'écriture formatée, affichez en une seule ligne les variables a, b et c dont
    les valeurs sont respectivement la chaîne de caractères "salut", le nombre entier
    102 et le float 10.318. La variable c sera affichée avec 2 décimales.

    '''
    )

    a = "salut"
    b = 102
    c = 10.318

    print(f'''
    Reponse:
    -------

    {a} {b} {c}
    '''
    )